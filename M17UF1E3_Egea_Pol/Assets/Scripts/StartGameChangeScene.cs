using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartGameChangeScene : MonoBehaviour
{
    public TMPro.TMP_InputField field;
    public GameObject datajugador;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(taskOnClick);
    }

    // Update is called once per frame
    void taskOnClick()
    {
        if (field.text != "") {
            datajugador.transform.parent = null;
            DontDestroyOnLoad(datajugador);
            SceneManager.LoadScene("GameScreen");
        }
        else Debug.LogError("No s'ha posat nom");
    }
}
