using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum tipusPers
{
    mag,
    healer,
    duelista,
    entry
}
public class PlayerData : MonoBehaviour
{
    public string PlayerName;
    public float Height;
    public tipusPers tipuPersonatge;
    private int _comptasprites;
    [SerializeField]
    private int _years = 20;
    public Sprite[] sprites ;
    // Start is called before the first frame update
    private float _comptaFrames;
    public float weight;
    private float weightmultiplier;
    public int distance;
    private int _actDistance;
    int timer=300;
    public int actualtimer;
    public float velocitat;
    float _fpsmax;
    void Start()
    {
        velocitat = 0.15f;
        _fpsmax = 0;
        _actDistance = 0;
        weightmultiplier = 1f;
        CheckWeight();
        _comptaFrames = 0;
        _comptasprites = 0;
        Debug.Log(transform.position);
        Debug.Log(this.gameObject.GetComponent<SpriteRenderer>().sprite);
        if(GameObject.Find("dataPlayer")!=null)PlayerName = GameObject.Find("dataPlayer").GetComponent<TMPro.TMP_InputField>().text;
    }

    // Update is called once per frame
    void Update()
    {
        _fpsmax=(Time.deltaTime - _fpsmax) * 0.1f;
        float fps = 1.0f /_fpsmax/12.0f;
        _comptaFrames +=1;
        if (_comptaFrames >= fps)
        {
            if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
            {
                PersRotationByKeyboard();
                transform.position += new Vector3(Input.GetAxis("Horizontal") * (velocitat), Input.GetAxis("Vertical") * (velocitat), 0);

            }
            else
            {
                RecDistance();
            }
            ChangeSprite();
            _comptaFrames = 0;
        }
    }
    public void PersRotationByKeyboard()
    {
        if (!CheckToRotate()) transform.rotation = new Quaternion(transform.rotation.x, 180, transform.rotation.z, transform.rotation.w);
        else transform.rotation = new Quaternion(transform.rotation.x, 0, transform.rotation.z, transform.rotation.w);
    }
    public void RecDistance() {
        _actDistance+=1;
        if (_actDistance == distance)
        {
            if (actualtimer != timer)
            {
                actualtimer += 1;
                _actDistance -= 1;
            }
            else
            {
                _actDistance = 0;
                actualtimer = 0;
            }

        }
        else if (_actDistance <= (distance / 2)) {
            transform.rotation = new Quaternion(transform.rotation.x, 180, transform.rotation.z, transform.rotation.w);
            transform.position += new Vector3((velocitat)*weightmultiplier, transform.position.y, 0); }
        else {
            transform.rotation = new Quaternion(transform.rotation.x, 0, transform.rotation.z, transform.rotation.w);
            transform.position += new Vector3(-(velocitat)*weightmultiplier, transform.position.y, 0); }
    }
    public bool CheckToRotate()
    {
        if (Input.GetAxis("Horizontal") < 0) return true;
        else return false;
    }
    public void CheckWeight()
    {
        if (weight > 100) for (float x = weight - 100f; x != 0; x--) weightmultiplier -= 0.01f;
        if (weight < 100) for (float x = weight - 100f; x != 0; x++) weightmultiplier += 0.01f;
    }
    public void ChangeSprite()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = sprites[_comptasprites];
        _comptasprites = (_comptasprites != sprites.Length - 1) ? _comptasprites +1 : 0;
    }
}
